# livecode-2

## Petunjuk
Telah disediakan dua halaman html : home.html, article.html dan folder vendor dan css
0. buatlah sebuah project laravel baru. setelah jadi, copy paste folder vendor dan folder css ke dalam folder public di project laravel.
1. Buatlah route '/home', '/articles'.
2. halaman home dan article merupakan dari satu template html yang sama. 
3. gunakan templating blade untuk membuat halaman home dan article menggunakan template app.blade.php yang sama. 
4. Penggunaan controller opsional, boleh pakai atau boleh tidak pakai controller.

Selamat mengerjakan!

